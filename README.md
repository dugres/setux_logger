# Setux Logger

[Setux] default logger

[PyPI] - [Repo] - [Doc]


[PyPI]: https://pypi.org/project/setux_logger
[Repo]: https://gitlab.com/dugres/setux_logger
[Doc]: https://setux-logger.readthedocs.io/en/latest
[Setux]: https://setux.readthedocs.io/en/latest
