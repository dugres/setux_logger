# Setux Logger
`setux.logger.logger`

[Setux] default logger

[PyPI] - [Repo] - [Doc]

Setux [Logger] instance

`setux.core.logger.Logger`

## Installation

    $ pip install setux_logger


[PyPI]: https://pypi.org/project/setux_logger
[Repo]: https://gitlab.com/dugres/setux_logger
[Doc]: https://setux-logger.readthedocs.io/en/latest
[Setux]: https://setux.readthedocs.io/en/latest

[Logger]: https://setux-core.readthedocs.io/en/latest/logger
